package com.upnvj.lahan.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;
import com.upnvj.lahan.R;
import com.upnvj.lahan.homepage.tab.TabActivity;

public class RegisterActivity extends AppCompatActivity {
    EditText EdtEmail, EdtName, EdtNim, EdtPassword;
    Button BtnRegisterRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        BtnRegisterRegister = (Button) findViewById(R.id.btn_register_register);
        EdtEmail = (EditText)findViewById(R.id.edt_email_register);
        EdtName = (EditText)findViewById(R.id.edt_name_register);
        EdtNim = (EditText)findViewById(R.id.edt_nim_register);
        EdtPassword = (EditText)findViewById(R.id.edt_password_register);
        //dropdown fakultas
        Spinner spinner= (Spinner)findViewById(R.id.spinner_faculty_register);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.fakultas_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        BtnRegisterRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));

            }
        });
    }
}