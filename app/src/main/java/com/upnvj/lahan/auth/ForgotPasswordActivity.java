package com.upnvj.lahan.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.upnvj.lahan.R;


public class ForgotPasswordActivity extends AppCompatActivity {
    EditText EdtForgot;
    Button BtnForgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        EdtForgot = (EditText) findViewById(R.id.edt_email_forget_password);
        BtnForgot = (Button)findViewById(R.id.btn_forget_password);
    }
}