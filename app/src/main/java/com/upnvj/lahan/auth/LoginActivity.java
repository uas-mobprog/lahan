package com.upnvj.lahan.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.View;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.upnvj.lahan.R;
import com.upnvj.lahan.homepage.tab.TabActivity;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {
    //inisasi Edittext dan button
    Button BtnLoginLogin;
    EditText EdtEmailLogin,EdtPasswordLogin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        BtnLoginLogin = (Button) findViewById(R.id.btn_login_login);
        EdtEmailLogin = (EditText) findViewById(R.id.edt_email_login);
        EdtPasswordLogin = (EditText) findViewById(R.id.edt_password_login);

        TextView TvlupaPass = (TextView)findViewById(R.id.tv_forgot_password_login);
        TvlupaPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
            }
        });
        TextView TvRegister = (TextView)findViewById(R.id.tv_register_right_login);
        TvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
        BtnLoginLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, TabActivity.class));

            }
        });
    }
}