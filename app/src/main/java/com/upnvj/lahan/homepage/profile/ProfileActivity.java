package com.upnvj.lahan.homepage.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.upnvj.lahan.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}